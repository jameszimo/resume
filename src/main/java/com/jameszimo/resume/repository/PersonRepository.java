package com.jameszimo.resume.repository;

import com.jameszimo.resume.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> findByLastName(String name);

    Person findByEmail(String email);
}
