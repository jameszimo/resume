package com.jameszimo.resume.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class HttpConfig {

    @Bean
    public Filter httpsEnforcerFilter() {
        return new HttpsEnforcer();
    }
}
