package com.jameszimo.resume.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping({"/","//**/{spring:\\w+}"})
    public String index() {
        return "forward:/index.html";
    }
}
