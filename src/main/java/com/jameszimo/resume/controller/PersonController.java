package com.jameszimo.resume.controller;

import com.jameszimo.resume.domain.Person;
import com.jameszimo.resume.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@AllArgsConstructor
@RestController
@RequestMapping("/api")
public class PersonController {

    PersonRepository personRepository;

    @GetMapping("/people")
    public List<Person> people() {
        return personRepository.findAll();
    }

    @GetMapping("/person")
    public List<Person> peopleByLastName(@RequestParam(value = "lastName") String lastName) {
        return personRepository.findByLastName(lastName);
    }

    @GetMapping("/personByEmail")
    public Person personByEmail(@RequestParam(value = "email") String email) {
        return personRepository.findByEmail(email);
    }

    @GetMapping("/person/{id}")
    public Person personById(@PathVariable("id") Long id) {
        return personRepository.findById(id).orElse(null);
    }

}
