INSERT INTO person (id, city, email, first_name, job_title, last_name, phone, profile, state)
VALUES (1, 'Seattle', 'jameszimo@gmail.com', 'James', 'Senior Software Developer', 'Zimowsky', '206-552-9511', 'Self-motivated, hardworking, and dynamic team player who has a history of developing innovative technical solutions to complex problems. Culturally sensitive, well-traveled, and familiar working in diverse environments with both on-premise and off-shore teammates. In-depth knowledge of IT systems, the full development cycle, and numerous programming languages. Experience working with Lean and Agile methodologies in a high-energy environment with tight deadlines and exacting customers. Experience mentoring and coaching team members to ensure skills are passed on and that all team members feel included and empowered.', 'WA');

INSERT INTO competency (id, image_name, link, name) VALUES (1, 'openJdk.jpg', 'https://openjdk.java.net/', 'Java');
INSERT INTO competency (id, image_name, link, name) VALUES (2, 'springBoot.png', 'https://spring.io/', 'Spring Boot');
INSERT INTO competency (id, image_name, link, name) VALUES (3, 'react.png', 'https://reactjs.org/', 'React');
INSERT INTO competency (id, image_name, link, name) VALUES (4, 'angular.png', 'https://angular.io/', 'Angular');
INSERT INTO competency (id, image_name, link, name) VALUES (5, 'javascript.png', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript', 'JavaScript');
INSERT INTO competency (id, image_name, link, name) VALUES (6, 'html5.png', 'https://en.wikipedia.org/wiki/HTML5', 'HTML');
INSERT INTO competency (id, image_name, link, name) VALUES (7, 'sql.webp', 'https://en.wikipedia.org/wiki/SQL', 'SQL');
INSERT INTO competency (id, image_name, link, name) VALUES (8, 'bash.jpg', 'https://en.wikipedia.org/wiki/Scripting_language', 'Scripting (Bash, Windows)');
INSERT INTO competency (id, image_name, link, name) VALUES (9, 'git.png', 'https://git-scm.com/', 'Git');
INSERT INTO competency (id, image_name, link, name) VALUES (10, 'subversion.png', 'https://subversion.apache.org/', 'Subversion');
INSERT INTO competency (id, image_name, link, name) VALUES (11, 'pcf.png', 'https://run.pivotal.io/', 'Pivotal Cloud Foundry (PCF)');
INSERT INTO competency (id, image_name, link, name) VALUES (12, null, 'https://en.wikipedia.org/wiki/Test-driven_development', 'Test Driven Development (TDD)');
INSERT INTO competency (id, image_name, link, name) VALUES (13, null, 'https://en.wikipedia.org/wiki/Pair_programming', 'Pair Programming');
INSERT INTO competency (id, image_name, link, name) VALUES (14, 'json.png', 'https://www.json.org/', 'JSON');
INSERT INTO competency (id, image_name, link, name) VALUES (15, 'gradle.png', 'https://gradle.org/', 'Gradle');
INSERT INTO competency (id, image_name, link, name) VALUES (16, 'liquibase.svg', 'https://www.liquibase.org/', 'Liquibase');
INSERT INTO competency (id, image_name, link, name) VALUES (17, 'jira.jpg', 'https://www.atlassian.com/software/jira', 'Jira');
INSERT INTO competency (id, image_name, link, name) VALUES (18, 'gitlab.webp', 'https://about.gitlab.com/', 'GitLab');
INSERT INTO competency (id, image_name, link, name) VALUES (19, null, 'https://en.wikipedia.org/wiki/CI/CD', 'CI/CD');
INSERT INTO competency (id, image_name, link, name) VALUES (33, 'typescript.svg', 'https://www.typescriptlang.org/', 'Typescript');
INSERT INTO competency (id, image_name, link, name) VALUES (34, null, 'https://microservices.io/', 'Microservices');

INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 1);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 2);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 3);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 4);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 5);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 6);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 7);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 8);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 9);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 11);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 12);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 13);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 14);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 15);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 16);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 17);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 18);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 19);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 33);
INSERT INTO person_competencies (person_id, competency_id) VALUES (1, 34);

INSERT INTO experience (id, city, company, company_link, date_range, description, person_id, title) VALUES (4, 'Seattle, WA', 'The Boeing Company', 'https://boeing.com', '2004 – 2015', 'Developed ETL, interactive dashboards, reports, and analysis templates using Oracle Business Intelligence Enterprise Edition (OBIEE)|Analyzed, designed, coded, and implemented complex solutions for an integrated Siebel CRM client/server information system|Developed and maintained a J2EE (Java) web service interface to Oracle Siebel CRM that allows an always available system even during Siebel outages|Worked with functional analysts, architects, offshore development and other IT groups in order to design changes to software applications and to minimize risk during the development lifecycle|Lead On-shore and off-shore resources in projects to develop technical solutions to highly complex business requirements|Automated common software development processes (repository export, compile, tests, server builds) in order to decrease errors and decrease software move time which saved countless rework hours and money. |Lead configuration management activity to implement ClearCase, then Subversion as source control and to automate code move activities in an effort to reduce errors and save time and money|Provided third level production support for an integrated Oracle Siebel CRM implementation', 1, 'Software Developer - Oracle Siebel CRM/Oracle Business Intelligence (OBIEE)');
INSERT INTO experience (id, city, company, company_link, date_range, description, person_id, title) VALUES (3, 'Seattle, WA', 'The Boeing Company', 'https://boeing.com', '2015 - 2017', 'Developed and implemented software solutions for a J2EE (Java) based analytics application that presents report data from a data warehouse to airline customers and Boeing internal users|Worked with users, functional analysts, architects, offshore development and other IT groups in order to design changes to application and to minimize risk during the development lifecycle|Modernized development tools and methodology for legacy application to include Agile, Git, Maven, Artifactory, VersionOne and Rundeck|Determined specifications and requirements for upcoming changes with other integrated applications and future technology stack for application going forward|Conducted application architecture activities including security assessments, disaster recovery planning and participated in other IT compliance exercises|Coached and mentored team members to ensure skills were passed on and to set up team members for successful software development activities', 1, 'Software Developer - Fleet Reliability Solutions Tool');
INSERT INTO experience (id, city, company, company_link, date_range, description, person_id, title) VALUES (1, 'Seattle, WA', 'The Boeing Company', 'https://boeing.com', '2018 – Present', 'Empower client development teams in modern software development practices by working with them side by side while developing software solutions for Cloud Foundry based web applications at multiple Boeing sites and at Pivotal Labs in Seattle|Practice pair programming, test driven development (TDD) and other eXtreme programming techniques on small balanced teams in order to accomplish iterative development and continuous delivery (CI/CD) of features and stories for end users while enabling client development teams|Lead a team as the ''Anchor'' developer on an engagement with an HR IT team on a large project to replace an outgoing HR system, which involved interfacing with management and working with other senior developers in order to make technical architecture decisions and to provide leadership for the team', 1, 'Senior Software Developer - Full Stack - Digital Transformation Environment (DTE)');
INSERT INTO experience (id, city, company, company_link, date_range, description, person_id, title) VALUES (2, 'Seattle, WA', 'The Boeing Company', 'https://boeing.com', '2017 - 2017', 'Developed software solutions for an Azure cloud based web application utilizing a microservices architecture|Practiced pair programming and test driven development (TDD) in an agile development setting to accomplish iterative and continuous delivery (CI/CD) of features and stories for end users|Worked with product owners, functional analysts, offshore and other on premise development teams, architects, and end users in order to coordinate development on various libraries and services for a complex cloud based web application', 1, 'Software Developer - Full Stack - Toolbox Project');

INSERT INTO education (id, city, degree, link, person_id, school, state) VALUES (1, 'Moscow', 'Bachelor of Science, Chemistry', 'https://www.uidaho.edu/', 1, 'University of Idaho', 'Idaho');
INSERT INTO education (id, city, degree, link, person_id, school, state) VALUES (2, 'Seattle', 'Certificate, Java Programming', 'https://www.pce.uw.edu/', 1, 'University of Washington', 'Washington');