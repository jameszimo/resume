package com.jameszimo.resume.controller;

import com.jameszimo.resume.domain.Person;
import com.jameszimo.resume.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonRepository mockPersonRepository;

    @Test
    void getPeople_shouldReturnAllPeople() throws Exception {

        when(mockPersonRepository.findAll()).thenReturn(Arrays.asList(
                Person.builder().id(1L).build(),
                Person.builder().id(2L).build()
        ));

        this.mockMvc.perform(get("/api/people"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"id\":1,")))
                .andExpect(content().string(containsString("\"id\":2,")));

        verify(mockPersonRepository).findAll();
        verifyNoMoreInteractions(mockPersonRepository);
    }

    @Test
    void getPersonByLastName_shouldReturnAllPeopleWithSpecifiedLastName() throws Exception {

        when(mockPersonRepository.findByLastName(anyString())).thenReturn(Arrays.asList(
                Person.builder().id(1L).firstName("Agent").lastName("Smith").build(),
                Person.builder().id(2L).firstName("Matt").lastName("Smith").build()
        ));

        this.mockMvc.perform(get("/api/person?lastName=Smith"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Agent")))
                .andExpect(content().string(containsString("Matt")));

        verify(mockPersonRepository).findByLastName("Smith");
        verifyNoMoreInteractions(mockPersonRepository);
    }

    @Test
    void getPersonByEmail_shouldReturnPersonWithSpecifiedEmail() throws Exception {

        when(mockPersonRepository.findByEmail(anyString())).thenReturn(
                Person.builder().id(1L).email("agent.smith@gmail.com").firstName("Agent").lastName("Smith").build()
        );

        this.mockMvc.perform(get("/api/personByEmail?email=agent.smith@gmail.com"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Agent")));

        verify(mockPersonRepository).findByEmail("agent.smith@gmail.com");
        verifyNoMoreInteractions(mockPersonRepository);
    }

    @Test
    void getPersonById_shouldReturnPersonWithSpecifiedId() throws Exception {

        when(mockPersonRepository.findById(anyLong())).thenReturn(
                Optional.of(
                        Person.builder().id(1L).firstName("Agent").lastName("Smith").build()
                ));

        this.mockMvc.perform(get("/api/person/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Agent")));

        verify(mockPersonRepository).findById(1L);
        verifyNoMoreInteractions(mockPersonRepository);
    }
}