import {TestBed} from '@angular/core/testing';
import {PersonService} from './person.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Person} from '../resume/domain/person';
import {environment} from '../../environments/environment';

describe('PersonService', () => {

  let service: PersonService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(PersonService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return ResumePerson when getResumePerson is called', () => {
    const expectedPerson = new Person({
      firstName: 'John',
      lastName: 'Smith',
    });

    service.getResumePerson().subscribe(result => {
      expect(result).toEqual(expectedPerson);
    });

    const request = httpTestingController
      .expectOne(req => req.method === 'GET' && req.url === environment.baseUrl + PersonService.GET_PERSON_ENDPOINT);
    request.flush(expectedPerson);

  });

});
