import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Person} from '../resume/domain/person';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  public static readonly GET_PERSON_ENDPOINT = '/api/person/1';

  // tslint:disable-next-line:variable-name
  private readonly _person: Observable<Person>;

  constructor(private http: HttpClient) {
    this._person = this.http.get(environment.baseUrl + PersonService.GET_PERSON_ENDPOINT)
      .pipe(
        map(person => new Person(person)),
        shareReplay(1)
      );
  }

  getResumePerson(): Observable<Person> {
    return this._person;
  }

}
