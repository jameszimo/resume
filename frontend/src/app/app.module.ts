import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BannerComponent} from './resume/banner/banner.component';
import {ProfileComponent} from './resume/profile/profile.component';
import {ExperienceComponent} from './resume/work-experience/experience/experience.component';
import {WorkExperienceComponent} from './resume/work-experience/work-experience.component';
import {HttpClientModule} from '@angular/common/http';
import {ResumeComponent} from './resume/resume.component';
import {EducationComponent} from './resume/education/education.component';
import {CompetenciesComponent} from './resume/competencies/competencies.component';
import {MatIconModule} from '@angular/material/icon';
import {SourceControlComponent} from './resume/source-control/source-control.component';

@NgModule({
  declarations: [
    AppComponent,
    ResumeComponent,
    BannerComponent,
    ProfileComponent,
    ExperienceComponent,
    WorkExperienceComponent,
    EducationComponent,
    CompetenciesComponent,
    SourceControlComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
